#!/bin/sh

function init(){
git init
} 

function add(){
echo "Введите имя файла, который нужно проиндексировать:"
read name
git add $name
}

function commit(){
echo "Напишите комментарий к текущему коммиту"
read comment
git commit -m "$comment" 
}

function status(){
git status
}

function branchvet(){
echo "Напишите название ветки:"
read k
git branch "$k" 
}

function branchc(){
git branch
}

function branchcom(){
git branch -v
}

function branchrm(){
echo "Напишите название ветки:"
read rom
git branch -d "$rom"
}

function branchiadd(){
echo "Напишите название файла, котрый хотите удалить из индекса:"
read nameiadd
git rm --cached "$nameiadd"
}

function branchadd(){
echo "Напишите название ветки, котрую хотите удалить:"
read nameadd
git branch -d "$nameadd"
}

function com(){
git log
}

function sin(){
echo "Напишите имя удаленного сервера для синхронизации с локальным:"
read sin
git remote add $sin https://89994695007@bitbucket.org/89994695007/android.git
}

function push(){
echo "Для отправления коммитов убедитесь, что вы находитесь в нужной ветки, введите имя ветки, что бы на нее переключиться:"
read power
git checkout $power
echo "Напишите имя удаленного сервера:"
read glob
echo "Напишите имя ветки, котрую хотите отправить:"
read glob1
git push $glob $glob1
}

function pull(){
echo "Напишите имя удаленного сервера:"
read g
echo "Напишите имя ветки, которую запрашиваете:"
read g1
git pull $g $g1
}

echo "Укажите путь для создания локального репозитория:"
read path
cd $path


echo "1.Работа с репозиторием"
echo "2.Работа с ветками"
echo "3.Работа с файлами"
echo "4.Работа с коммитом"
echo "5.Работа с удаленным сервером"
read n
case $n in
1)
echo "Создать репозиторий:"
echo "1.Да"
echo "2.Нет"
echo "3.Статус состояния репозитория"
read m
case $m in
1)
init
echo "Репозиторий успешно создан"
;;
2)
exit 
;;
3)
status
;;
esac
;;
2)
echo "1.Создание ветки"
echo "2.Статус веток"
echo "3.Просмотр последних коммитов на каждой из веток"
echo "4.Удаление индексированного файла с дополнительным сохранением на винчестер"
echo "5.Удалить ветку"
read t
case $t in
1)
branch11
echo "Ветка успешно создана"
;;
2)
branchc
;;
3)
branchcom
;;
4)
branchiadd
echo "Удаление индекса, с сохранением на винчестер, прошло успешно"
;;
5)
branchadd
echo "Ветка успешно удалена"
;;
esac
;;
3)
add
echo "Файл успешно проиндексирован"
;;
4)
echo "Сделать снимок текущего процесса, сейчас?"
echo "1.Да"
echo "2.Нет"
echo "3.Показать список действующих коммитов "
read foto
case $foto in
1)
commit
echo "Снимок текущего процесса успешно выполнен"
;;
2)
exit
;;
3)
com
;;
esac
;;
5)
sin
echo"Серверы успешно синхронизированы"
echo
echo "1.Поделиться локальным репозиторием"
echo "2.Отправить запрос на глобальный репозиторий"
read mak
case $mak in
1)
push
echo "Ветка успешно отправлена"
;;
2)
pull
echo "Ветка успешно получена"
;;
esac
;;
esac
exit
