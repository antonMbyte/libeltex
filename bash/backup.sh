#!/bin/sh

CLIENT_SERVER(){
echo "Введите имя каталога или файла с удаленного сервера:"
read serv
echo "Введите имя каталога на локальной машине:"	
read loc
echo "$serv\n$loc" >> textfile
}

job_with_server(){
echo "Передача данных осуществялется по безопасному протоколу SSH\n"
echo "Введите имя пользователя удаленной системы:"
read user
echo "$user" > textfile
echo "Введите IP-адресс пользователя:"
read ip
echo "$ip" >> textfile
}

backup(){
USER=`cat /home/anton/local/textfile | sed -n '1p'`
IP=`cat /home/anton/local/textfile | sed -n '2p'`
SERV=`cat /home/anton/local/textfile | sed -n '3p'`
LOC=`cat /home/anton/local/textfile | sed -n '4p'`
nice -n +17 rsync -avzh -e ssh "$USER"@"$IP":/home/$USER/"$SERV" /home/$USER/"$LOC"
}

backup1(){
USER=`cat /home/anton/local/textfile | sed -n '1p'`
IP=`cat /home/anton/local/textfile | sed -n '2p'`
SERV=`cat /home/anton/local/textfile | sed -n '3p'`
LOC=`cat /home/anton/local/textfile | sed -n '4p'`
nice -n +17 rsync -avzh -e ssh /home/anton/"$LOC" "$USER"@"$IP":/home/anton/"$SERV"
}

CRON(){
echo "Переодичность выполнения Backup:"
echo "\n1.Раз в год\n2.Раз в месяц\n3.Раз в неделю\n4.Раз в день\n5.Раз в час\n6.Задать свое время"
read what
case $what in
1)
echo "0 0 1 1 *  /home/anton/local/backup.sh $number" >> textfile | crontab textfile 
;;
2)
echo "0 0 1 * *   /home/anton/local/backup.sh $number" >> textfile | crontab textfile
;;
3)
echo "0 0 * * 0   /home/anton/local/backup.sh $number" >> textfile | crontab textfile
;;
4)
echo "0 0 * * *   /home/anton/local/backup.sh $number" >> textfile | crontab textfile
;;
5)
echo "0 * * * *   /home/anton/local/backup.sh $number" >> textfile | crontab textfile
;;
6)
echo "Задайте пять первых параметров времени через пробел:"
read parametr
echo "$parametr /home/anton/local/backup.sh $number" >> textfile | crontab textfile
;;
esac	
}

case_backup(){
echo "\nРезервное копирование:\n\n1.На удаленый сервер\n2.С удаленного сервера\n3.На внешнее устройство\n4.С внешнего устройства"
read number
case $number in
1)
CRON
job_with_server
CLIENT_SERVER
;;
2)
CRON
job_with_server
CLIENT_SERVER
;;
3)
echo
;;
4)
echo
;;
esac
}

case_upr(){
echo "Главное меню Backup:"
echo  "\n1.Создать процесс\n2.Редактировать процесс\n3.Удалить процесс\n4.Выход"
read name
case $name in
1)
case_backup
;;
2)
;;
3)
;;
4)
exit
;;
esac
}



#argument=$1
#if [ "$argument" != '' ] 
#then

#else 
#echo  "1.Создать\2.Редактировать\n2.Выход"
#read name

case $1 in
1)
backup1
;;
2)
backup
;;
*)
rm /home/anton/local/textfile
case_upr
;;
esac
exit

