#!/bin/sh
### BEGIN INIT INFO
# Provides:          skeleton
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Example initscript
# Description:       This file should be used to construct scripts to be
#                    placed in /etc/init.d.
### END INIT INFO


#Написать скрипт, который проверяет смонтирована ли папка по nfs и при необходимости
#ее монтирует, в папку он помещает файл, в котором написано: имя фамилия ip адрес время
#запуска \ остановка uptime. скрипт должен выполняться при запуске и остановке машины. Для
#выполнения задания необходимо пользоваться вохможностями upstart и system.d

timesystem(){
STR=$(uptime | awk '{print($3)}' | wc -c)
if [[ "$STR" -eq  6 ]] 
then
TIME=$(uptime | awk '{print($3)}' | sed 's/,//')
else
TIME=$(uptime | awk '{print($3)}')
TIME=$(echo "$TIME  min")
fi 
	}

update(){
H=$(date '+%H')
M=$(date '+%M') 
S=$(date '+%S')
Y=$(date '+%Y')
}
	
do_start(){
touch /home/anton/nfsmount/disk1/nfs.log
mv /home/anton/nfsmount/disk1/nfs.log /home/anton/
sudo mount -t nfs 127.0.0.1:/home/anton/samsung /home/anton/nfsmount/disk1
update
timesystem
echo -e "USER: Anton Vorozhischev\nIP-адресс: 127.0.0.1\nПродолжительность работы: "$TIME"\nВремя запуска: "$H:$M:$S  $Y"\nПапка по nfs успешно смонтирована\n" >> /home/anton/nfs.log
mv /home/anton/nfs.log /home/anton/nfsmount/disk1/
}
	
do_stop(){
mv /home/anton/nfsmount/disk1/nfs.log /home/anton/
sudo umount nfs 127.0.0.1:/home/anton/samsung /home/anton/nfsmount/disk1
update
timesystem
echo -e "USER: Anton Vorozhischev\nIP-адресс: 127.0.0.1\nПродолжительность работы: "$TIME" \nВремя остановки: "$H:$M:$S  $Y"\nПапка по nfs успешно демонтирована\n" >> /home/anton/nfs.log
mv /home/anton/nfs.log /home/anton/nfsmount/disk1/
}

case $1 in

start)
do_start
;;

stop)
do_stop
;;

esac
exit
	
