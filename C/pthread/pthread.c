/*-------------------------------------------------------------------------------------------
Warcraft. Заданное количество юнитов добывают золото равными порциями из одной шахты, 
задерживаясь в пути на случайное время, до ее истощения. 
Работа каждого юнита реализуется в порожденном процессе.
-------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <wait.h> 
#include <unistd.h>
#include <sys/types.h>


/*Делаем глобальный объект мьютекс, доступный всем потокам*/
pthread_mutex_t mutex; 

 /*Задем структуру с информацией о текущей глубине шахты*/
struct data{
	short number_pthread;
    short Depth_of_shovel;
    short Depth_of_mine;
};

/*Модифицируем тип данных структуры*/
typedef struct data Threads_Struct;

/*Деклалрируем структрцную переменную*/
Threads_Struct depthofMine; 

/*Потоковая функция*/
void* Digging_Gold(void *arg)
{   
    Threads_Struct *p = (Threads_Struct*) arg;

    /*Выполнять, пока а*/
    while((p -> Depth_of_mine) > 0)
    {
    pthread_mutex_lock(&mutex);
    sleep(rand()%5);

    p -> Depth_of_mine -= p -> Depth_of_shovel; 
     if ((p -> Depth_of_mine) <= 0)
    	{
    		printf("The mine is empty\n");
    		pthread_mutex_unlock(&mutex);
    		pthread_exit(0);
    	}
    printf("Count gold of mine = %d\n", p -> Depth_of_mine);
    pthread_mutex_unlock(&mutex);
	}
	
    return NULL;
}
 
 
 
int main(int argc, char **argv)
{
 
    if (argc != 4)
    {
    	fprintf(stdout, "%s\n", "ERROR_ARG: Few arguments");
    	fprintf(stdout, "%s\n", "Example: <Number of units>, <Depth of mine - cm>, <Depth of shovel - cm>");
    }

   /*Декларируем массив под идентификаторы потоков*/
    pthread_t threads[atoi(argv[1])];


    /*Начальная инициализация полей структуры*/
   	depthofMine.Depth_of_mine = atoi(argv[2]);
   	depthofMine.Depth_of_shovel = atoi(argv[3]); 	

    /*Создаем(инициализируем) объект мютекса*/
    pthread_mutex_init(&mutex, NULL);


    for (unsigned short i = 0; i < atoi(argv[1]); i++)
    {
        pthread_create(&threads[i], NULL, Digging_Gold, &depthofMine);
    }


    for (unsigned short i = 0; i < atoi(argv[1]); i++) 
    {
        pthread_join(threads[i], NULL);
    }

    /*Уничтожаем объект мютекс*/
    pthread_mutex_destroy(&mutex);
    return 0;
}