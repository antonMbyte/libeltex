/*----------------------------------------
Удалить из текста заданный символ 
Параметры командной строки:
  1. Имя входного файла 
  2. Заданный символ
-----------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>   

void fileProcessing (FILE *file, FILE *outfile, char symbol, int j, char *buffer, char *symbolIN);
void freeMemory(FILE *file, FILE *outfile, char *buffer, char *bufferString);

int main(int argc, char *argv[]){
	if (argc < 4){
    printf("Нет входных параметров. Пример использования : %s <имя входного файла> <символ> <расширение выходного файла>\n", argv[0]);
    exit(0);
	}
   else
   	{  
       char *symbolIN = argv[2];
   	   int j = 0;
   	   char symbol = 0, *buffer = NULL;
       FILE *file = fopen(argv[1], "r");    //открываю файл и связываю его с потоком (возврат адрес начала файла в памяти)
       if(file == NULL)
       {
       	 printf("Ошибка. Невозможно открыть файл.\n");
       	 exit(1);
       }      
       else
       {   
            char *tochaMas= strchr(argv[1], '.');     // возвращаю указатель на символ '.'
       	    int n = (tochaMas - argv[1]) + 1;    //  ((указатель точка - указатель нулевого элемнта) + точка) итог:filestring.
       	    char *bufferString = malloc(sizeof(char) * n);  //выделяю память под массиы filestring.
       	    for (int j = 0; j < n; j++)
       	     {
       	      bufferString[j] = argv[1][j];         //собственно заполнение массива
       	     } 
       	     strcat(bufferString, argv[3]);         // копирую указатель на 3 параметр(строку) в новый указатель на строку

       	  FILE *outfile = fopen(bufferString, "w"); // открываю выходной файл для записи

       	  fileProcessing(file, outfile ,symbol, j, buffer, symbolIN);    
          printf("Данные обработанны и записаны в файл %s\n", bufferString);
          freeMemory(file, outfile, buffer, bufferString);  
       	   
       }
   	}
   	return 0;
 }




void fileProcessing (FILE *file, FILE *outfile, char symbol, int j, char *buffer, char *symbolIN){

 printf("Файл успешно открыт и связан с потоком\n");    	
 while((symbol=fgetc(file)) != EOF){              // читаю посимвольно с файла до тех пор пока не встречу конец файла(смещаю указатель)
      if (symbol != *symbolIN)                    // сравниваю прочитанный символ, с символом с командной строки 
      { 
         buffer = malloc(sizeof(char));           //выделяю память в буфере под символ
         fseek( file , -1 , SEEK_CUR );           //перевожу текущий указатель на 1 бит назад
         fread(buffer, sizeof(char), 1, file);    //читаю из файла посимвольной в буффер
         if(ferror(file) != 0){                //проверка на ошибку чтения файла
          printf("Ошибка чтения входного файла\n");
          exit(1);
        }
         fwrite(buffer, sizeof(char), 1, outfile); //пишу с буффера в файл
         if(ferror(file) != 0){
          printf("Ошибка записи выходного файла\n");
          exit(1);
        } 
      }
    
    }
        return ;    //возврат управления вызывающей функции
}



void freeMemory(FILE *file, FILE *outfile, char *buffer, char *bufferString){
  fclose(file);
  fclose(outfile);
  free(buffer);
  free(bufferString);
  return ;
}