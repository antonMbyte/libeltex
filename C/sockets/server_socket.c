/*---------------------------------------------------------------------------------------
 Поиск указанной строки в указанном файле. Обработка одной строки в порожденном процессе.
----------------------------------------------------------------------------------------*/
#include <stdio.h> 		
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include "struct_data.h"

/*Декларация струтктурной переменной для адрессной структуры*/
sock_addr_t sockAddr;

int main(int argc, char **argv)
{
	 int listen_socket, connect_socket, numline; 
	 char buffer_data[MAX_BUFFER]; //буффер под чтение данных из сокета
	

	/*Создание сокета (специального файла в ядре)*/
	if ((listen_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
		{
			perror("Can't create socket: \n");
			exit(EXIT_FAILURE);
		} 

		/*Инициализация полей адрессной структуры для дальнейшей привязке к сокету*/
		sockAddr.sin_family = AF_INET;  
		sockAddr.sin_port = htons(12345); 
		sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

		/*Привязка адрессной струтуры к сокету*/
		if (bind(listen_socket, (struct sockaddr *)(&sockAddr), sizeof(sockAddr)) == -1)
		{
			perror("Can't bind: \n");
			close(listen_socket);
			exit(EXIT_FAILURE);
		}

			/*Сокет начинает слушать извне запросы на соединения*/
			if (listen(listen_socket, SOMAXCONN) == -1)
			{
				perror("Can't listen: \n");
				close(listen_socket);
				exit(EXIT_FAILURE);
			}

	for( ; ; )
	{	
		/*Сокет начинает принимать соединения и блокируеться если их нет*/
		if ((connect_socket = accept(listen_socket, 0, 0)) == -1)
				{
					perror("Can't accept: \n");
					close(connect_socket);
					exit(EXIT_FAILURE);
				}

			/*Чтение из сокет(из потока ввода/вывода по его идентификатору)*/
			if (recv(connect_socket, buffer_data, sizeof(recv_server_data_t), MSG_NOSIGNAL) == -1)
				{
					perror("Can't read: ");
					close(connect_socket);
					exit(EXIT_FAILURE);
				}

				/*Приводим тип буффера char к типу клиентской струтуры данных*/
				recv_server_data_t *data = (recv_server_data_t*)buffer_data;

				/*Функция возвращает номер найденной строки в файле*/
				numline = Search_String(data);
				send_server_data.numlen = numline;
			
			if (send(connect_socket, &send_server_data, sizeof(send_server_data_t), MSG_NOSIGNAL) == -1)
				{
					perror("Can't read: ");
					close(connect_socket);
					exit(EXIT_FAILURE);
				}
      
				close(connect_socket);
	}

	return 0;
}

