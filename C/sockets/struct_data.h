
/*максимальный размер буффера*/
#define MAX_BUFFER 1024
#define MAX_STRLEN 256

/*Струтуры для отправки и приема клиентского процесса */
struct recv_client_DATA
{
	int numlen;
	char linebuf[MAX_BUFFER];
}recv_client_data;

struct send_client_DATA
{
	char name_file[MAX_BUFFER];	
	char string[MAX_BUFFER];
	int i;
}send_client_data;


/*Струтуры для отправки и приема серверного процесса */
struct recv_server_DATA
{
	char name_file[MAX_BUFFER];	
	char string[MAX_BUFFER];
	int i;
}recv_server_data;

struct send_server_DATA
{
	int numlen;
	char linebuf[MAX_BUFFER];
}send_server_data; 


/*Переопределение типов структур для клиентского процесса*/
typedef struct send_client_DATA send_client_data_t;
typedef struct recv_client_DATA recv_client_data_t;

/*Переопределение типов структур для серверного процесса*/
typedef struct send_server_DATA send_server_data_t;
typedef struct recv_server_DATA recv_server_data_t;

/*Переопределение типов структур адрессов для клиентского и сеоверного процесса*/
typedef struct sockaddr_in sock_addr_t;

/*Интерфейсы между процессом и функцией*/
int Search_String(recv_server_data_t *);
void *Connection(void *);