#include <stdio.h> 		
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include "struct_data.h"

/*Функция поиска принятой от клиента строки. Возвращет номер строки в файле*/
int Search_String(recv_server_data_t *data)
{
	char buf_string[MAX_BUFFER];
	char symbol;
	FILE *fd;
	int s = 0, numlen = 0;
	if((fd = fopen(data->name_file, "r")) == NULL)
	{
		printf("Error: Can't open %s\n", data->name_file);
		exit(EXIT_FAILURE);
	}

	while((symbol = fgetc(fd)) != EOF)
	{
		buf_string[s++] = symbol;
		if(symbol == '\n')
		{
			numlen++;
			if((strcmp(data -> string, buf_string) == 0))
			{	
				sleep(1);
				printf("Отправка найденной строки...\n");
				strcpy(send_server_data.linebuf, buf_string);
				memset(buf_string, 0, sizeof(buf_string));
				fclose(fd);
				return numlen;
			}
			else {
				memset(buf_string, 0, sizeof(buf_string));
				s = 0;
			}
		}
	}
	fclose(fd);
 return 0;
}
