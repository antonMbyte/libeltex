#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <wait.h> 	
#include <stdlib.h>
#include "struct_data.h"

/*Декларация глобального объекта мютекса доступный всем потокам*/
pthread_mutex_t mutex;

/*Декларация струтктурной переменной для адрессной структуры*/
sock_addr_t sockAddr;

/*Потоковая функция для клиенских подключений*/
void *Connection(void *arg)
{
	pthread_mutex_lock(&mutex);
	send_client_data_t *data = (send_client_data_t*)arg;
	int connect_socket;
	char buffer[MAX_BUFFER];

		if ((connect_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
		{
			perror("Can't create socket: ");
			exit(EXIT_FAILURE);
		}

		if (connect(connect_socket, (struct sockaddr*) &sockAddr, sizeof (sockAddr)) == -1) 
		{
			perror("Can't connect to server: ");
			close(connect_socket);
			exit(EXIT_FAILURE);
		}

			
		if (send(connect_socket, data, sizeof(send_client_data_t), MSG_NOSIGNAL) == -1)
		{
			perror("Can't send: ");
			close(connect_socket);
			exit(EXIT_FAILURE);
		}
	
					
		if (recv(connect_socket, buffer, sizeof(recv_client_data_t), MSG_NOSIGNAL) == -1)
		{
			perror("Can't send: ");
			close(connect_socket);
			exit(EXIT_FAILURE);
		}
		
		
				
			recv_client_data_t *p = (recv_client_data_t*)buffer;
		
		if (p->numlen != 0)
		{
			printf("Строка **%s** есть в файле %s ---> Номер строки: %i \n", p->linebuf, data->name_file, p->numlen);
			close(connect_socket);
			pthread_mutex_unlock(&mutex);
		}
		else
		{ 
			printf("Нет такой строки!\n");
			close(connect_socket);
			pthread_mutex_unlock(&mutex);
		}
	
		
	return NULL;
}