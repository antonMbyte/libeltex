/*---------------------------------------------------------------------------------------
 Поиск указанной строки в указанном файле. Обработка одной строки в порожденном процессе.
----------------------------------------------------------------------------------------*/
#include <stdio.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <wait.h> 	
#include <stdlib.h>
#include "struct_data.h"


/*Декларация глобального объекта мютекса доступный всем потокам*/
pthread_mutex_t mutex;

/*Декларация струтктурной переменной для адрессной структуры*/
sock_addr_t sockAddr;

int main(int argc, char **argv)
{
	int count; //Колличество искомых строк 
	send_client_data_t *data = NULL; //указатель на массив струтктур
	char buf_name_file[MAX_BUFFER], buf_string[MAX_BUFFER]; // один буффер для имени обрабатываемого файла, второй для искомой строки

	/*Проверка на входные параметры: IP-адресс и порт получателя*/
	if (argc != 3)
	{
		fprintf(stderr, "%s\n", "Few arguments: <For example>: <IP-adress> <PORT>");
		exit(EXIT_FAILURE);
	}
	/*Проверка на приоритетный ввод*/
	else if (strlen(argv[1]) < strlen(argv[2]))
	{
		fprintf(stderr, "%s\n", "<Priority error>: <For example>: <IP-adress> <PORT>");
		exit(EXIT_FAILURE);
	}

	/*Инициализация полей струтктуры аргументами с командной строки*/
	sockAddr.sin_family = AF_INET;  					
	sockAddr.sin_port = htons(atoi(argv[2]));	 		
	sockAddr.sin_addr.s_addr = inet_addr(argv[1]);
	pthread_mutex_init(&mutex, NULL);	 	

	/*Получения имени файла и колличества строк со стандартного потока ввода*/
	fprintf(stdout, "%s", "Enter the name of file: ");	
	scanf("%s", buf_name_file);	
	fprintf(stdout, "%s", "Enter the number of search strings: ");
	scanf("%i\n", &count);
	pthread_t pthread_id[count];

	/*Код для отправки одной строки*/
	if (count == 1)
	{	
		fgets(buf_string, MAX_STRLEN, stdin);
		strcpy(send_client_data.name_file, buf_name_file);
		strcpy(send_client_data.string, buf_string);
		send_client_data.i = count;
		pthread_create(&pthread_id[count], NULL, Connection, &send_client_data);
		memset(buf_string, 0, sizeof(buf_string));
	}
	/*Код для отправки нескольких строк*/
	else
	{
		data = malloc(sizeof(send_client_data_t) * count);

		for (int i = 1; i <= count; i++)
		{
			fgets(buf_string, MAX_STRLEN, stdin); //получение со стандаотного потока ввода строки
			strcpy(data[i].name_file, buf_name_file); //копирование имени файла в буффер структуры 
			strcpy(data[i].string, buf_string); // копирование строки в буффер струтуры
			pthread_create(&pthread_id[i], NULL, Connection, &data[i]); //создаем потоки
			memset(buf_string, 0, sizeof(buf_string)); //Зануляем буффер для следующей строки
		}

		/*Поток-родитель ждет завершение дочерних потоков и присоединяет поток к главному*/
    	for (int j = 1; j <= count; j++) 
	   	{
       		pthread_join(pthread_id[j], NULL);
   		}


	}
	/*ожидание одиночного потока*/
	pthread_join(pthread_id[count], NULL);

		return 0;
}
