#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#define MAX_STRING 1000


void printMas(char **mas, int sizeMas, int i, int j);


int stringMax(char **mas, int sizeMas, int i, int j)
{
  char Max = strlen(mas[0]);
    for (i = 0; i < sizeMas; ++i)
    {
      for (j = 0; j < sizeMas; ++j)
      {
        if (strlen(mas[j]) > strlen(mas[i]))

        Max = strlen(mas[j]);
      }
    }
   
     return Max;
}


int stringMin(char **mas, int sizeMas, int i, int j)
{
  int Min = strlen(mas[0]);
    for (i = 0; i < sizeMas; ++i)
    {
      for (j = 0; j < sizeMas; ++j)
      {
        if (strlen(mas[j]) < strlen(mas[i]))

        Min = strlen(mas[j]);
      }
    }
   
     return Min;
}




void MinMaxString (char **mas, int sizeMas, int i, int j)
{ 
  int string = 0;
  printf("%s\n%s\n%s\n%s\n", "What is the length of the line?", "1. The largest", "2. The smallest", "3. Exit", "Enter number = ");
  scanf("%d", &string);
  switch (string) 
  {
case 1:
  printMas(mas, sizeMas, i, j);
  printf("%s%d\n", "Length of the largest line = ", stringMax(mas, sizeMas, i, j));
  break;
case 2:
  printMas(mas, sizeMas, i, j);
  printf("%s%d\n", "Length of the smallest line = ", stringMin(mas, sizeMas, i, j));
  break;
default:
  return ;
  break;
}
return ;
}



int sortMasDiminution (char **mas, int sizeMas, int i, int j, int tmp, int k)
{
 for (i = 0; i < sizeMas; ++i)
  {  
    for (j = i + 1; j < sizeMas; ++j)
    { 
      if (strlen(mas[j]) > strlen(mas[i]))
      {
        tmp = mas[j];
        mas[j] = mas[i];
        mas[i] = tmp;
        k += 1;
      }
    }
  }

  return k;
}




int sortMasIncrease (char **mas, int sizeMas, int i, int j, int tmp, int k)
{
  for (i = 0; i < sizeMas; ++i)
  {  
    for (j = i + 1; j < sizeMas; ++j)
    { 
      if (strlen(mas[j]) < strlen(mas[i]))
      {
        tmp = *(mas + j); 
        *(mas + j) = *(mas + i); 
        *(mas + i) = tmp; 
         k += 1;
      }
    }
  }

  return k;
}






char** readMas (char **mas, int sizeMas, int i)
{
  char buffer[MAX_STRING];
  mas = malloc(sizeof(char**) * sizeMas);
  printf("%45s\n", "Input array:");
  for (i = 0; i < sizeMas; ++i)
  {
    printf("mas[%d] = ", i);
    scanf("%s", buffer);
    mas[i] = malloc(sizeof(char*) * strlen(buffer));
    strcpy(mas[i], buffer);
  }

  return mas;
}




int sortMas (char **mas, int sizeMas, int i, int j, int tmp, int k)
{
  int string;
  printf("\n\n%s\n%s\n%s\n%s\n%s", "How to Sort an Array?", "1. Ascending", "2. Descending", "3. Exit", "Enter number = ");
  scanf("%d", &string);
  switch (string) 
  {
case 1:
  k = sortMasIncrease(mas, sizeMas, i, j, tmp, k);
  break;
case 2:
  k = sortMasDiminution(mas, sizeMas, i, j, tmp, k);
  break;
default:
  return ;
  break;
}
return k;
}




void printMas(char **mas, int sizeMas, int i, int j)
{
  printf("%50s\n", "Processing result: ");
  for (i = 0; i < sizeMas; ++i)
  {
    printf("mas[%d] = ", i);
    printf("%s\n", mas[i]);
  }
  return ;
}




void freeMas (char **mas, int sizeMas, int i)
{
    for (i = 0; i < sizeMas; ++i)
    {
      free(mas[i]);
    }

    free(mas);

  return ;
}




int main (void)
{

 int i = 0, j = 0, k = 0;
 char **mas = NULL;
 int tmp = 0;
 int sizeMas = 0;
 printf("Enter the quantity of elements in the array = ");
 scanf("%d", &sizeMas);
 mas = readMas(mas ,sizeMas, i);
 k = sortMas(mas, sizeMas, i, j, tmp, k);
 MinMaxString(mas, sizeMas, i, j);
 printf("%s%d\n", "Quantity of permutations = ", k);
 freeMas(mas, sizeMas, i);
 return 0 ;

}