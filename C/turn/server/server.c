/*------------------------------------------------------------
Статистический анализ. 
Имеется несколько массивов данных (разного размера). 
Требуется определить математическое ожидание в каждом массиве. 
Обработка каждого массива выполняется в отдельном процессе.
------------------------------------------------------------*/
#include <sys/types.h> //прописаны определения системных типов
#include <sys/stat.h>  
#include <wait.h>    //прописан интерфейс для ожидания дочерних процессов
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#include "define.h"
#include "data_mass.h"

//структура сообщения(буффер под сообщение и его тип)
struct msgbuf{
        long mtype;
        int countMassData;
        float result[MSG_LEN];
}msgbuf;

pid_t PID;   // тут лежит идентификатор процесса-себя
int writefd; //дескриптор для записи
int msgID;   //дескриптор очереди на запись-чтение
int **MassPoint = NULL;
float MathExp;
int pidoff, count = 0;

int main(void){

if ((msgID = msgget(MSG_KEY, MSG_FLAG)) == -1){
		perror("msgget");
		exit(-3);
}

if (msgrcv(msgID, &msgbuf, sizeof(int), 1, 0) < 0) {
        perror("msgrcv");
        exit(-4);
 }

printf("Обработка данных...\n");
sleep(4);
printf("Данные отправлены клиенту\n");

int* N = malloc(sizeof(int) * msgbuf.countMassData);
int fd[msgbuf.countMassData][2];//разобраться !! ошибка
pid_t forkPid[msgbuf.countMassData];

//инициализация неименованного канала
for (int i = 0; i < msgbuf.countMassData; i++){
if(pipe(fd[i])==-1){
printf("Can not create chanel[%d]", i);
exit(-5);
	}
}

MassPoint = fillMass(MassPoint, msgbuf.countMassData, N); //записываю адрес на случайный массив
//создаю дочерние процессы 
for (int i = 0; i < msgbuf.countMassData; i++){
forkPid[i] = fork();

//проверка на ошибку
if (forkPid[i] == -1){
printf("Can not create forkPid[%d]", i);
       exit(-5);
}

//дочерний процесс стратует
else if (forkPid[i] == 0){
close(fd[i][0]);  //потомок закрывает все концы канала для чтения
MathExp = CalculateProcess(MassPoint, i, N);
write(fd[i][1], &MathExp, sizeof(float));
exit(1);
}
}

do{    //3. Родительский процесс стартует
    for (int i = 0; i < msgbuf.countMassData; i++){
      pidoff = waitpid(forkPid[i], 0, WUNTRACED | WCONTINUED);
     	if (pidoff == forkPid[i]){
     		close(fd[i][1]);    //родитель закрывает конец канала для записи
     		read(fd[i][0], &msgbuf.result[i], sizeof(float));
            count++;
                                          
            }
        }
} while( count != msgbuf.countMassData);
msgbuf.mtype = 5;
msgsnd(msgID, &msgbuf, sizeof(msgbuf), IPC_NOWAIT); 

return 0;
}




