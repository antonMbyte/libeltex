/*------------------------------------------------------------
Статистический анализ. 
Имеется несколько массивов данных (разного размера). 
Требуется определить математическое ожидание в каждом массиве. 
Обработка каждого массива выполняется в отдельном процессе.

Программа клиент.
------------------------------------------------------------*/
#include <sys/types.h> //прописаны определения системных типов
#include <sys/stat.h>  
#include <wait.h>    //прописан интерфейс для ожидания дочерних процессов
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h> 
#include "define.h"


//структура хранит информацию об отправленном сообщении в очередь
struct msgbuf{
        long mtype;
        int countMassData;
        float result[MSG_LEN];
}msgbuf;


int readfd; //тут лежит дескриптор для чтения из одного конца канала(FIFO)
int PIDbuffer; //сюда записывается идентификатор сервера-процесса
int msgID;     //дескриптор очереди 
size_t bufLength; //длина буффера сообщения

//ТОЧКА ВХОДА
int main(void){

printf("Соединение с сервером...\n");
sleep(3);
printf("Кол-во массивов данных подлежащих обработки:\nK = ");
scanf("%d", &msgbuf.countMassData);	


//иницализация очереди по ключу(возврат дескриптор)
if((msgID = msgget(MSG_KEY, MSG_FLAG)) == -1){
		perror("msgget");
		exit(-2);
}

msgbuf.mtype = 1; //индексирую сообщение


if (msgsnd(msgID, &msgbuf, sizeof(int), IPC_NOWAIT) < 0) { //отправляем сообщение в очередь
        perror("msgsnd");
        exit(-3);
    }


msgrcv(msgID, &msgbuf, sizeof(msgbuf), 5, 0 );
msgctl(msgID, IPC_RMID, 0);

printf("Сервер обрабатывает запрос...\n");
sleep(3);
for (int i = 0; i < msgbuf.countMassData; i++){
	printf("Mathematic expectation[%d] = %.2f \n", i + 1, msgbuf.result[i]);
}


return 0;
}