/*------------------------------------------------------------------------------------------------------------------------------------
Голодные игры. Родительский процесс создает указанное количество дочерних, фиксируя их идентификаторы (например, в файле). 
Каждый дочерний процесс удаляет из списка произвольный идентификатор, если он не равен его собственному и отправляет системный запрос 
на уничтожение другого процесса, чей идентификатор он выбрал. 
Игра прекращается. когда останется один участник, родительский процесс сообщает идентификатор победителя.
-------------------------------------------------------------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>

int checkLife(int *);
int findTarget(int *);
int checkMas(int *);

int num;

int main(void){

union semun{
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
};
    

  key_t key = 70;
  int *shm, shmid, semid, count, result, q;
  union semun arg;   
 
  /* заполянем поля струтктуры противоположными состояниями.*/
  struct sembuf block_res = {0, -1, SEM_UNDO};  //блокировка ресурса
  struct sembuf unblock_res = {0, 1, SEM_UNDO};  //освобождение ресурса
 
  printf("Введите кол-во процессов = ");
  scanf("%d", &count);
  num = count;
  int size = sizeof(int) * count;
  int pid[count];

 /* Получим ключ, Один и тот же ключ можно использовать как
    для семафора, так и для разделяемой памяти */
  if ((key = ftok(".", 'S')) < 0) {
    printf("Невозможно получить ключ\n");
    exit(1);
  }

/* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
  semid = semget(key, 1, 0666 | IPC_CREAT);

/* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
  arg.val = 1;
  semctl(semid, 0, SETVAL, arg);


    /* Создадим область разделяемой памяти в адресном пространстве ядра */
    if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    /* Привяжем или разместим сегмент в адресном пространстве процесса */
    if ((shm = shmat(shmid, NULL, 0)) == (int *) -1) {
        perror("shmat");
        exit(1);
    }


   for (int i = 0; i < count; i++){
      
      
      pid[i] = fork();

      /* Parent process */
        if(pid[i] != 0){
         /* Write identification of process in sharid memory */
          for (int i = 0; i < count; i++){
              shm[i] = pid[i];
        }
   }

 /* Child process */
    if (0 == pid[i]) 
    {
        sleep(2);
        int target = 0;
    
            while(checkMas(shm) != 1){
        
                /* Заблокируем доступ к ресурсу текущим процессом */  
                if((semop(semid, &block_res, 1)) == -1)
                {
                    fprintf(stderr, "Lock failed\n");
                    exit(1);
                }    
                else
                {
                    printf("----------------------------\n");
                    printf("Семафор заблокировал ресурс\n");
                    fflush(stdout);
                }
                    printf("PID%d: Ищу жертву...\n", getpid());
  

    if (-1 == checkLife(shm))
    {
        printf("process is dead\n");
        exit(-1);
    }
    else
    {
        if (-1 == (target = findTarget(shm)))
        {
            printf("this winner\n");
            exit(0);
        }
        else
        {
            printf("Убит PID%d\n", shm[target]);
            kill(shm[target], SIGKILL);
            shm[target] = 0;

        }

    }

    /* Освобождаем ресурс */
    if((semop(semid, &unblock_res, 1)) == -1)
    {
        fprintf(stderr, "Unlock failed\n");
           exit(2);
    } 
    else
    {
        printf("Семафор освободил ресурсы\n");
        printf("----------------------------\n");
        fflush(stdout);
    }
    }
   
    exit(0);
}
        else if (pid[i] < 0)
        {
          perror("fork"); /* произошла ошибка */
            exit(4); /*выход из родительского процесса*/
        } 
  
 }
  

    /* Родитель ждет завершения процессов */
    do{
        for (int i = 0; i < count; i++)
        {
                result = waitpid(pid[i], 0, WUNTRACED | WCONTINUED);  //жду потомок завершит процесс
                    if (result == pid[i])
                    {
                        q++;                                     
                    }
        }
    } while (q != count);

    
    /* Вывод победителя */
    for (int i = 0; i < count; i++)
    {
        if (shm[i] != 0)
        {
            printf("\n\nID победителя = %d\n\n", shm[i]);
        }
    }


    /* Отключаем память от адресного пространства процесса */
    if (shmdt(shm) < 0) 
    {
         printf("Error OFF sharid memory\n");
            exit(3);
    } 

   
    /* Удаляем сегмент памяти из ядра*/
    if (shmctl(shmid, IPC_RMID, 0) < 0) 
    {
    printf("Ошибка(разделяемая память): Невозможно удалить сегмент.\n");
    exit(5);
    } 
       else
            {
                 printf("Сегмент памяти успешно удален из системы.\n");
            } 

  
    /* Удаляем семафор из ядра*/
    if (semctl(semid, 0, IPC_RMID) < 0) 
    {
    printf("Ошибка(семафор): Невозможно удалить семафор.\n");
    exit(6);
    } 
    else
    {
         printf("Семафор успешно удален из системы.\n");
    }
        return 0;
    }


    /*Функция проверяет на жизнеспособность процесс*/
    int checkLife(int *shm)
    {
        for(int i = 0; i < num; i++)
        {
            printf("shm[%d] = %d\n", i,shm[i]);
        }

        for(int i = 0; i < num; i++)
        {
            if (shm[i] == getpid()) 
            {
            return 0;
            }
        }
            printf("PID%d is dead", getpid());
            return -1;
    }

    /*Функция для поиска жертвы*/
    int findTarget(int *shm)
    {
        srand(getpid());
        int target = 0;
            if (checkMas(shm) == 1) return -1;
        do
        {
            target = rand()%num;
        }while((getpid() == shm[target]) | (shm[target] == 0));

        return target;
    }

    /*Функция для подсчета кол-ва элементов массива*/
    int checkMas(int *shm)
    {
        int length = num;
            for(int i = 0; i < num; i++)
            {
                if (shm[i] == 0)
                {
                    length--;
                }
            }
            return length;
    }