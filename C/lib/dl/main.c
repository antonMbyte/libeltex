#include <dlfcn.h>
#include "stdio.h"
#include "mathematic.h"
int main(void) {
int n1 = 4, n2 = 4;
int m1 = 4, m2 = 2;
//char *messg = "Hello world!\n";
   // Открываем совместно используемую библиотеку
   void *dl_handle = dlopen( "/home/anton/Eltex/Linux/C/lib/drlib/libmathcad.so", RTLD_LAZY );
   //  Находим адрес функции в библиотеке
   int (*ummath)(int, int);
   int (*dmmath)(int, int);
   ummath = dlsym( dl_handle, "um" );
   dmmath = dlsym( dl_handle, "del" );
   
   // Вызываем функцию по найденному адресу
    printf("%d\n", (*ummath)(n1, n2));
    printf("%d\n", (*dmmath)(m1, m2));
   // Закрываем библиотеку
   dlclose( dl_handle );
   return 0;
}